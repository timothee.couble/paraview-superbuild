commit 02c816720af9e648790012cbd79db91c4d9d8f61
Author: Ben Boeckel <ben.boeckel@kitware.com>
Date:   Tue Sep 13 20:05:09 2022 -0400

    mpi: support using MS-MPI
    
    MS-MPI does not use an `mpicc` wrapper and instead provides its
    information through environment variables. When requested, use this
    information. Also requires `HDF5_MPI=ON` to avoid requesting MS-MPI in
    non-MPI contexts.

diff --git a/docs/build.rst b/docs/build.rst
index 7eb8221b..2cfb83a7 100644
--- a/docs/build.rst
+++ b/docs/build.rst
@@ -238,3 +238,7 @@ by setting the ``HDF5_MPI`` environment variable::
 
 You will need a shared-library build of Parallel HDF5 as well, i.e. built with
 ``./configure --enable-shared --enable-parallel``.
+
+On Windows, MS-MPI is usually used which does not have an ``mpicc`` wrapper.
+Instead, you may use the ``H5PY_MPI`` environment variable to ``ON`` in order
+to query the system for MS-MPI's information.
diff --git a/setup_build.py b/setup_build.py
index 5bf2a634..23c75042 100644
--- a/setup_build.py
+++ b/setup_build.py
@@ -91,6 +91,11 @@ class h5py_build_ext(build_ext):
         settings['library_dirs'][:0] = config.hdf5_libdirs
         settings['define_macros'].extend(config.hdf5_define_macros)
 
+        if config.msmpi:
+            settings['include_dirs'].extend(config.msmpi_inc_dirs)
+            settings['library_dirs'].extend(config.msmpi_lib_dirs)
+            settings['libraries'].append('msmpi')
+
         try:
             numpy_includes = numpy.get_include()
         except AttributeError:
diff --git a/setup_configure.py b/setup_configure.py
index d96f238b..e8bf0147 100644
--- a/setup_configure.py
+++ b/setup_configure.py
@@ -64,6 +64,20 @@ class BuildConfig:
         self.ros3 = ros3
         self.direct_vfd = direct_vfd
 
+        if self.mpi and os.environ.get('H5PY_MSMPI') == 'ON':
+            self.msmpi = True
+            self.msmpi_inc_dirs = os.environ.get('MSMPI_INC').split(';')
+            import platform
+            if platform.architecture()[0] == '64bit':
+                mpi_lib_envvar = 'MSMPI_LIB64'
+            else:
+                mpi_lib_envvar = 'MSMPI_LIB32'
+            self.msmpi_lib_dirs = os.environ.get(mpi_lib_envvar).split(';')
+        else:
+            self.msmpi = False
+            self.msmpi_inc_dirs = []
+            self.msmpi_lib_dirs = []
+
     @classmethod
     def from_env(cls):
         mpi = mpi_enabled()
@@ -175,6 +189,9 @@ class BuildConfig:
             'mpi': self.mpi,
             'ros3': self.ros3,
             'direct_vfd': self.direct_vfd,
+            'msmpi': self.msmpi,
+            'msmpi_inc_dirs': self.msmpi_inc_dirs,
+            'msmpi_lib_dirs': self.msmpi_lib_dirs,
         }
 
     def changed(self):
@@ -192,13 +209,16 @@ class BuildConfig:
         print('*' * 80)
         print(' ' * 23 + "Summary of the h5py configuration")
         print('')
-        print("HDF5 include dirs:", fmt_dirs(self.hdf5_includedirs))
-        print("HDF5 library dirs:", fmt_dirs(self.hdf5_libdirs))
-        print("     HDF5 Version:", repr(self.hdf5_version))
-        print("      MPI Enabled:", self.mpi)
-        print(" ROS3 VFD Enabled:", self.ros3)
-        print("DIRECT VFD Enabled:", self.direct_vfd)
-        print(" Rebuild Required:", self.changed())
+        print("  HDF5 include dirs:", fmt_dirs(self.hdf5_includedirs))
+        print("  HDF5 library dirs:", fmt_dirs(self.hdf5_libdirs))
+        print("       HDF5 Version:", repr(self.hdf5_version))
+        print("        MPI Enabled:", self.mpi)
+        print("   ROS3 VFD Enabled:", self.ros3)
+        print(" DIRECT VFD Enabled:", self.direct_vfd)
+        print("   Rebuild Required:", self.changed())
+        print("     MS-MPI Enabled:", self.msmpi)
+        print("MS-MPI include dirs:", self.msmpi_inc_dirs)
+        print("MS-MPI library dirs:", self.msmpi_lib_dirs)
         print('')
         print('*' * 80)
 
